export interface Athlete {
  name: string,
  team: string,
  JerseyNo: number,
  stillPlaying: boolean
}
