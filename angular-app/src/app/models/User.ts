export interface User {
  firstName: string,
  lastName: string,
  age: number,
  email?: string,
  image?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any
}

// NOTE: ? - makes the property optional/not required
