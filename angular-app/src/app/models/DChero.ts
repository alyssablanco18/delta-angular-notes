export interface DChero {
  persona: string,
  firstname: string,
  lastname: string,
  age: number,
  address?: {
    street: string,
    city: string,
    state: string
  },
  img?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any,
  hide?: boolean
}
