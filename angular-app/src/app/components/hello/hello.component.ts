// 1. Import the Component library from @angular/core
import {Component, OnInit} from "@angular/core";

// 2. Create the selector and template using the @Component decorator
@Component({
  selector: 'app-hello',
  // template: `<h3>Apple</h3>`
  templateUrl:"hello.component.html",
  styleUrls:["hello.component.css"]
})

// 3. export the component class
export class helloComponent implements OnInit{
  ngOnInit() {
  }
}
