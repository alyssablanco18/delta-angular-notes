import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  // PROPERTIES
  goodWeather: string = "Sunny and cool";
  badWeather: string = "Cold and snow storm";


  constructor() { }

  ngOnInit(): void {
    // call the displayGoodWeatherMessage method
    // this.displayGoodWeatherMessage()

    // call displayBadWeatherMessage
    this.displayBadWeatherMessage()
  }

  // METHODS
  displayGoodWeatherMessage() {
    alert(`Todays Weather is ${this.goodWeather}`);
  }

  displayBadWeatherMessage() {
    alert(`Bundle up, Charge your phones, because today's
     weather is ${this.badWeather}`);
  }

}
