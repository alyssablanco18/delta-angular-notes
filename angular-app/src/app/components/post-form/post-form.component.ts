import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostsService} from "../../services/posts.service";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  // PROPERTIES
  // created a new property that's referencing our interface
  // set values to 0 or ''
  post: PostHttpClient = {
    id: 0,
    title: '',
    body:''
  };

  @Output() newPost: EventEmitter<PostHttpClient> = new EventEmitter()

  @Output() updatedPost: EventEmitter<PostHttpClient> = new EventEmitter()

  // defining current post as a input
  @Input() currentPost: PostHttpClient = {
    id:0,
    title:'',
    body:''
  };

  // defining 'isEditing' as another Input
  @Input() isEditing: boolean = false;

  // inject our dependency
  constructor(private postService: PostsService) { }

  ngOnInit(): void {
  }// end of ngOnInIt

  //METHODS

  // create a method named addPost() by using the PostServices savePost()
  // and attaching the event emitter
  addPost(title: any ,body: any) {
    if (!title || !body) {
      alert("Please complete the form")
    }
    else {
      console.log(title,body)
      this.postService.savePost({title,body} as PostHttpClient)
        .subscribe(data => {
          this.newPost.emit(data);
        })
    }
  }

  updatePost(){
    console.log("Updating post....");
    this.postService.updatePost(this.currentPost)
      .subscribe(data => {
        this.isEditing = false;
        this.updatedPost.emit(data);
      })
    this.currentPost = {
      id: 0,
      title:'',
      body:''
    }
  }


}
