import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  // PROPERTIES
  num1: number = 12;
  num2: number = 12;

  constructor() { }

  ngOnInit(): void {
      this.sum ();
      this.difference();
      this.product();
      this.quotient();
      this.fizzbuzz();
  }

  // METHODS
  sum(){
    return this.num1 + this.num2;
  }
  difference(){
    return this.num1 - this.num2;
  }
  product(){
    return this.num1 * this.num2;
  }
  quotient(){
    return this.num1 / this.num2;
  }
  // sum(){
  //   console.log(this.num1 + this.num2);
  // }

  // sum(a: any, b: any){
  //   this.num1 = a;
  //   this.num2 = b;
  //   console.log(`${a} + ${b} = ${a + b}`);
  // }
  //
  // difference(){
  //   return this.num1 - this.num2;
  // }
  //
  // produce(){
  //   console.log(this.num1 * this.num2);
  // }
  //
  // quotient(){
  //   console.log(this.num1 / this.num2);
  // }

  // FIZZBUZZ
  fizzbuzz(){
    for (var i = 1; i <= 100; i++){
      if (i % 3 === 0 && i % 5 === 0){
        console.log("FizzBuzz")
      }
      else if (i % 3 === 0){
        console.log("Fizz")
      }
      else if (i % 5 === 0){
        console.log("Buzz")
      }
      else{
        console.log(i);
      }
    }
  }


}



