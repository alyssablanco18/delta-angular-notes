import { Component, OnInit } from '@angular/core';
import {CommentsService} from "../../services/comments.service";
import {Comment} from "../../models/Comment";
@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  // properties
  // properties can have any name ex: listOfComments: Comment[] = [];
  comments: Comment[] = [];

  //for updating comment:
  selectedComment: Comment = {
    id: 0,
    name: "",
    email: "",
    body: ""
  }
  // property for update button:
  commentEdit: boolean = false;


  // dependency injection
  constructor(private commentsService: CommentsService) { }
  ngOnInit(): void {
    // subscribe to Observable method:
    this.commentsService.getComment().subscribe( data => {
      this.comments = data;
    })
  }
  // Methods:
  onSubmitComment(commentToAdd: Comment){
    this.comments.unshift(commentToAdd);
  }
  // create 2 methods: one for updating post one for
  // editing a post
  onUpdatedComment(editComment: Comment) {
    this.comments.forEach((current, index) =>{
      if (editComment.id === current.id){
        this.comments.splice(index,1);
        this.comments.unshift(editComment);
        this.commentEdit = false;
        this.selectedComment = {
          id: 0,
          name: "",
          email: "",
          body: "",
        }
      }
    });
  }
  editComment(comment: Comment) {
    this.selectedComment = comment;
    this.commentEdit = true;
  }
//  remove comment
  removeComment(commentToBeDeleted : Comment) {
    if (confirm("Are you sure?")) {
      this.commentsService.deleteComment(commentToBeDeleted.id)
        .subscribe(() => {
          this.comments.forEach((current, index) =>{
            if (commentToBeDeleted.id === current.id) {
              this.comments.splice(index, 1);
            }
          })
        })
    }
  }
} // end of class
