import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delta',
  templateUrl: './delta.component.html',
  styleUrls: ['./delta.component.css']
})
export class DeltaComponent implements OnInit {
  // PROPERTIES: AN ATTRIBUTE OF A COMPONENT
  firstName: string = "Delta";
  lastName: string = "Cohort";
  year: number = 2021;

  // called when component is initialized (used)
  // used for dependency injections - connection from the service(s)
  constructor() { }


  // called when component is initialized
  // used for reassigning and calling methods
  ngOnInit(): void {
    console.log("Hello from delta component...");
    console.log(`${this.firstName} ${this.lastName}`);

    // in order to access properties we need to use the 'this' keyword

    this.greeting()
    // In order to call a method, use the 'this' keyword
  }

  // METHODS - FUNCTION INSIDE OF A CLASS COMPONENTS CLASS
  greeting() {
    alert("Hello there, " + this.firstName);
    // calling displayLastName() to run here
    this.displayLastName()
    alert("The year is " + this.year);
    // *NOTE: to access properties in a method, use the 'this' keyword
  }

  displayLastName() {
    alert("last Name is " + this.lastName);
  }

} // end of the class

