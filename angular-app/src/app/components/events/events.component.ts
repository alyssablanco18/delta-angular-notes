import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  // PROPERTIES
  showForm: boolean = true; // will show the form

  constructor() { }

  ngOnInit(): void {
    this.showForm = false; // default to invisible form
  }; // end of ngOnInIt

  //METHODS
  // method will set showForm to true/false
  toggleForm() {
    console.log('Toggle form button was clicked')
    this.showForm = !this.showForm; // either will show/hide the form
  }

  // method for the different types of mouse events
  triggerEvent(event:any) {
    console.log(event.type);
  }


}
