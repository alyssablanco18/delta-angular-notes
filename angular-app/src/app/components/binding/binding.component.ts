import { Component, OnInit } from '@angular/core';
import {DChero} from "../../models/DChero";

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {
  // PROPERTIES
  characters: DChero[] = [];
  currentClasses: {} = {}; // an empty object
  currentStyle: {} = {};
  enableAddCharacter: boolean = true;

  constructor() { }

  ngOnInit(): void {

    // add data to the array of characters
   this.characters = [
     {
       persona:'Superman',
       firstname: 'Clark' ,
       lastname:'kent',
       age: 56,
       address: {
         street: '27 Smallville',
         city: 'Metropolis',
         state: 'IL'
       },
       img:"../assets/img/644-superman.jpg.jpg",
       isActive: true,
       balance: 12000000,
       memberSince: new Date("05/01/1939 8:30:00"),
       hide: false
     },
     {
       persona: "Wonder Woman",
       firstname: "Diana",
       lastname: "Prince",
       age: 5000,
       address:{
         street: `150 Main St`,
         city: "Los Angeles",
         state: "CA"
       },
       img: "../assets/img/wonderwoman.png",
       balance: 14000000,
       memberSince: new Date("07/11/2005 10:45:00"),
       hide: false
     },
     {
       persona: "Batman",
      firstname: "Bruce",
       lastname: "Wayne",
       age: 43,
       address:{
         street: `50 Wayne Manor`,
         city: "Gotham City",
         state: "NJ"
       },
       img: "../assets/img/batman.png",
       isActive: true,
       balance: 20000000,
       memberSince: new Date("08/07/1979 12:00:00"),
       hide: false
     }
   ]; // end of array

    this.setCurrentClasses();
    this.setCurrentStyle();

  }; // end of ngOnInIt()

  //  METHODS
  // EXAMPLE OF ngClass
  setCurrentClasses() {
    this.currentClasses = {
      'btn-success' : this.enableAddCharacter
    }
  };

  // create a method to toggle the individual characters info
  toggleInfo(character: any) {
    console.log('toggle info clicked');
    character.hide = !character.hide
  }

  // example of ng style
  setCurrentStyle() {
    // call and update the property currentStyle
    this.currentStyle = {
      "padding-tip": "60px",
      'text-decoration' : "underline"
    }
  }

} // end of class
