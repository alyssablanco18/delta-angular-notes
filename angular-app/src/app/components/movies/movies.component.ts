import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/Movie";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  // PROPERTIES
  movies: Movie[] = [];

  constructor() { }

  ngOnInit(): void {

    this.movies = [
      {
        title: 'Soul',
        yearReleased: 2020,
        actors:'Pete Docter,Kemp Powers and Mike Jones',
        director: 'Pete Docter',
        genre:'Family/Comedy',
        rating: 95,
        awards:'Best Animated film of 2021, Best film music of 2021 and Best sound'
      },
      {
        title: 'Coco',
        yearReleased: 2017,
        actors:'Anthony Gonzalez, Benjamin Bratt, Gael Garcia Bernal',
        director: 'Adrian Molina and Lee Unkrich',
        genre:'Family/Adventure',
        rating: 97,
        awards:'Academy Award for Best Animated Feature Film of 2018, Golden Globe Award' +
          'For Best Animated Film 2018'
      },
      {
        title: 'The Good Dinosaur',
        yearReleased: 2015,
        actors:'Raymond Ochoa, Sam Elliott, Steve Zahn',
        director: 'Peter Sohn',
        genre:'Family/Adventure',
        rating: 76,
        awards:'VES Awards'
      },
      {
        title: 'Finding Dory',
        yearReleased: 2016,
        actors:'Ellen Degeneres, Ty Burnell, Ed ONeill',
        director: 'Andrew Stanton',
        genre:'Family/Adventure',
        rating: 94,
        awards:'Kids Choice Awards 2017, Peoples Choice Award for Favorite Movie 2017'
      },
      {
        title: 'Moana',
        yearReleased: 2016,
        actors:'Raymond Ochoa, Sam Elliott, Steve Zahn',
        director: 'Ron Clements an John Musker',
        genre:'Family/Musical',
        rating: 95,
        awards:'Teen Choice Awards 2017'
      },
      {
        title: 'Finding Nemo',
        yearReleased: 2003,
        actors:'Albert Brooks, Ellen Degeneres, Alexander Gould',
        director: 'Andrew Stanton',
        genre:'Family/Adventure',
        rating: 99,
        awards:'Academy award for best Animated Feature Film 2004, AFI movies of the year 2003'
      },
      {
        title: 'The Incredibles',
        yearReleased: 2004,
        actors:'Brad Bird, Holly Hunter,Craig T Nelson, Samuel L Jackson ',
        director: 'Brad Bird',
        genre:'Family/Comedy',
        rating: 97,
        awards:'Academy Award for best animated feature film 2005, Academy Award for best sound editing 2005'
      },
      {
        title: 'Ratatouille',
        yearReleased: 2007,
        actors:'Patton Oswalt, Brad Garrett, Janeane Garefalo',
        director: 'Brad Bird',
        genre:'Family/Comedy',
        rating: 96,
        awards:'Academy Award for best Animated feature Film 2008, Golden Globe Award 2008'
      },
      {
        title: 'Cars',
        yearReleased: 2006,
        actors:'Owen Wilson, Larry the cable guy, Bonnie Hunt',
        director: 'John Lasseter',
        genre:'Family/Comedy',
        rating: 74,
        awards:'Golden Globe award for best Animated feature film 2008'
      },
    ];// end of array

    this.addMovie();
  }; // end of NgOnInIt

  // METHODS
  movierating() {
    return ' This Movie has very good reviews!'
  }

  addMovie(){
    this.movies.push({title: 'Zootopia', yearReleased: 2016, actors:'Jason Batemon, Shakira, Ginnifer Goodwin', director:'Bryan Howard and Rich Moore', genre: 'Family/Comedy', rating: 98,awards: 'Academy award for best animated feature film 2017, Kids choice award for favorite frenemies 2017' });
  }

}
