import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommentsService} from "../../services/comments.service";
import {Comment} from "../../models/Comment";
@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {
  // properties
  comment: Comment = {
    id: 0,
    name: '',
    email: '',
    body: ''
  }
  @Output() newComment: EventEmitter<Comment> = new EventEmitter();
  @Output() updatedComment: EventEmitter<Comment> = new EventEmitter();
  @Input() currentComment: Comment = {
    id: 0,
    name: '',
    email: '',
    body: ''
  }
  @Input() isEdit: boolean = false;
  // dependency injection
  constructor(private commentService: CommentsService) { }
  // 3 ways for creating a private variable for Service
  //private commentsService : CommentsService
  // private _commentService (variable name) : CommentsService
  // private commentSvc : CommentsService -- then change in method
  ngOnInit(): void {
  }
  // METHOD:
  addComment(name: any, email: any, body:any){
    if (!name || !email || !body){
      alert("Please complete the form")
    }
    else {
      console.log(name, email, body);
      this.commentService.saveComment({name, email, body} as Comment)
        .subscribe(data => {
          this.newComment.emit(data)
        })
    }
  }
  updateComment(){
    console.log("Updating comment....");
    this.commentService.updateComment(this.currentComment)
      .subscribe(data => {
        this.isEdit = false;
        this.updatedComment.emit(data);
      })
    this.currentComment = {
      id: 0,
      name: "",
      email: "",
      body: ""
    }
  }
}
