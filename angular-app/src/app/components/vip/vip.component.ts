import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {
  // PROPERTIES
  members: Member[] = []; // pointing to our member interface

  // BONUS
  loadingMembers: boolean = false;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {this.loadingMembers = true}, 5000);

    this.members = [
      {
        firstName: 'Constance',
        lastName: 'Sanders',
        username: 'ConSanders',
        memberNo: 25
      },
      {
        firstName: 'Susan',
        lastName: 'Adkins',
        username: 'Susiead',
        memberNo: 35
      },
      {
        firstName: 'Erica',
        lastName: 'Simmons',
        username: 'Esimmons',
        memberNo: 17
      },
      {
        firstName: 'Bonnie',
        lastName: 'Rivera',
        username: 'BonRivera',
        memberNo: 43
      },
      {
        firstName: 'Brandon',
        lastName: 'Taylor',
        username: 'BrandonT',
        memberNo: 52
      }
    ]
  }
}
