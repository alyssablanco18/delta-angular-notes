import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-string-interpolation',
  templateUrl: './string-interpolation.component.html',
  styleUrls: ['./string-interpolation.component.css']
})
export class StringInterpolationComponent implements OnInit {
  // PROPERTIES
  firstName: string = '';
  lastName: string = '';
  age: number = 0;


  constructor() { }

  ngOnInit(): void {
    this.firstName = 'Diana';
    this.lastName = 'Prince';
    this.age = 5000;
  }

  // METHOD
  sayHello() {
    return (`hello from ${this.firstName} ${this.lastName},
    and i am ${this.age} years old`)
  }

}
