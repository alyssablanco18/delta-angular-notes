import { Component, OnInit } from '@angular/core';
import {MarvelHero} from "../../models/MarvelHero";

@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.css']
})
export class NgmodelComponent implements OnInit {
  // PROPERTIES
  heroes: MarvelHero[] = [];

  // a property for the new character entry
  // - set each property of the object to an empty object
  character: MarvelHero = {
    persona:'',
    firstName:'',
    lastName:'',
    age:0
  }


  constructor() { }

  ngOnInit(): void {
    // add data to the heroes array
    this.heroes = [
      {
        persona: 'Captain America',
        firstName:'Steve',
        lastName:'Rogers',
        age: 77
      },
      {
        persona: 'Spider man',
        firstName:'Peter',
        lastName:'Parker',
        age: 17
      }
    ]; // end of array
  }; // end of ngOnInIt

  // METHODS
  // create a method to add a new character to the this.heroes array
  addCharacter() {
    // what do we need to do?
    this.heroes.unshift(this.character);

    // clear out the form/ reset the form...
    this.character = {
      persona:'',
      firstName:'',
      lastName:'',
      age: 0
    }
  }

}
