import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrls: ['./structural-directives.component.css']
})
export class StructuralDirectivesComponent implements OnInit {
  // PROPERTIES
  // CREATE A PROPERTY NAMED USERS, REFERENCE THE USER INTERFACE
  users: User[] = [];

  constructor() { }

  ngOnInit(): void {
    // add data to our property users
    this.users = [
      {
        firstName: "Clark",
        lastName: "kent",
        age: 55
      },
      {
        firstName:"Bruce",
        lastName: "Wayne",
        age: 43
      }
    ]
  }

}
