import { Component, OnInit } from '@angular/core';
import {Athlete} from "../../models/Athlete";

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {
  // PROPERTIES
  athletes: Athlete[] = []; // pointing to our athlete interface

  constructor() { }

  ngOnInit(): void {
    this.athletes = [
      {
        name: 'Michael Jordan',
        team: 'Chicago Bulls',
        JerseyNo: 23,
        stillPlaying: false
      },
      {
        name: 'Lebron James',
        team: 'Los Angeles Lakers',
        JerseyNo: 23,
        stillPlaying: true
      },
      {
        name: 'Lionel Messi',
        team: 'Barcelona',
        JerseyNo: 10,
        stillPlaying: true
      },
      {
        name: 'Alex Morgan',
        team: 'Orlando Pride',
        JerseyNo: 13,
        stillPlaying: true
      }
    ] // end of athletes array
  } // end of ngOnInIt

  // METHOD
  showStatus() {
    return "This athlete is currently playing";
  }

}
