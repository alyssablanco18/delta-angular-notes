import { Component, OnInit , ViewChild} from '@angular/core';
import {DChero} from "../../models/DChero";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  // PROPERTIES
  characters: DChero[] = [];
  currentClasses: {} = {}; // an empty object
  currentStyle: {} = {};
  enableAddCharacter: boolean = true;

  // character empty object
  character: DChero = {
    persona:'',
    firstname:'',
    lastname:'',
    age: 0 ,
    address: {
      street: '',
      city:'',
      state:''
    },
    img: '',
    isActive: true,
    memberSince:'',
    hide: true
  }

  // SUBMIT LESSON
  @ViewChild("characterForm") form: any;
  /*
  Calling ViewChild and passed in the name of of our form "characterForm"
  - making this our
   */

  // inject the data service in the constructor
  constructor(private dataService: DataService) { }
  /*
  Dependency injection
  private- cannot be used anywhere else , only within this class
  dataService- variable name for our DataService
  DataService- setting our variable to the DataService that we brought in
  Now we should be able to access any method(s) inside of our DataService
   */

  ngOnInit(): void {


    this.setCurrentClasses();
    this.setCurrentStyle();

    // access the getCharacters() method thats inside of our DataService
    this.dataService.getCharacters().subscribe(data => {
      this.characters = data;
    });
    // whenever we are accessing a Observable method,
    // we need to subscribe to it.


  }; // end of ngOnInIt()

  // create a method to toggle the individual characters info
  toggleInfo(character: any) {
    console.log('toggle info clicked');
    character.hide = !character.hide
  }

  // example of ng style
  setCurrentStyle() {
    // call and update the property currentStyle
    this.currentStyle = {
      "padding-tip": "60px",
      'text-decoration' : "underline"
    }
  }
  setCurrentClasses() {
    this.currentClasses = {
      'btn-success' : this.enableAddCharacter
    }
  };

  onSubmit({value,valid} : {value: DChero, valid: boolean}) {
    if (!valid){
      alert("form is not valid");
    }
    else {
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = false;
      console.log(value);

      // add the new data to the top of the list
      this.characters.unshift(value);

      // reset the form / clear out the inputs
      this.form.reset();
    }
  }
}
