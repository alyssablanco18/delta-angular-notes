import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { UserComponent } from "./components/user/user.component";
import { FruitComponent} from "./components/fruit/fruit.component";

import { helloComponent} from "./components/hello/hello.component";
import { travelComponent} from "./components/travel/travel.component";
import { DeltaComponent } from './components/delta/delta.component';
import { WeatherComponent } from './components/weather/weather.component';
import { MathComponent } from './components/math/math.component';
import { StringInterpolationComponent } from './components/string-interpolation/string-interpolation.component';
import { StructuralDirectivesComponent } from './components/structural-directives/structural-directives.component';
import { DirectivesComponent } from './components/directives/directives.component';
import { VipComponent } from './components/vip/vip.component';
import { BindingComponent } from './components/binding/binding.component';
import { MoviesComponent } from './components/movies/movies.component';
import { EventsComponent } from './components/events/events.component';
import { NgmodelComponent } from './components/ngmodel/ngmodel.component';
import {FormsModule} from "@angular/forms";
import { FormsComponent } from './components/forms/forms.component';
import { PostHttpClientComponent } from './components/post-http-client/post-http-client.component';
import { CommentsComponent } from './components/comments/comments.component';
import { PostFormComponent } from './components/post-form/post-form.component';
import { CommentFormComponent } from './components/comment-form/comment-form.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';

// declaration array- components
// imports array - modules
// providers array - services

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    FruitComponent,
    helloComponent,
    travelComponent,
    DeltaComponent,
    WeatherComponent,
    MathComponent,
    StringInterpolationComponent,
    StructuralDirectivesComponent,
    DirectivesComponent,
    VipComponent,
    BindingComponent,
    MoviesComponent,
    EventsComponent,
    NgmodelComponent,
    FormsComponent,
    PostHttpClientComponent,
    CommentsComponent,
    PostFormComponent,
    CommentFormComponent,
    HomeComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
