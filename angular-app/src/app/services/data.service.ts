import { Injectable } from '@angular/core';
import {DChero} from "../models/DChero";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // initialize the characters property and assign it an array of our DChero
  // interface
  characters: DChero[]= [];


  constructor() {
    this.characters = [
      {
        persona:'Superman',
        firstname: 'Clark' ,
        lastname:'kent',
        age: 56,
        address: {
          street: '27 Smallville',
          city: 'Metropolis',
          state: 'IL'
        },
        img:"../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstname: "Diana",
        lastname: "Prince",
        age: 5000,
        address:{
          street: `150 Main St`,
          city: "Los Angeles",
          state: "CA"
        },
        img: "../assets/img/wonderwoman.png",
        balance: 14000000,
        memberSince: new Date("07/11/2005 10:45:00"),
        hide: false
      },
      {
        persona: "Batman",
        firstname: "Bruce",
        lastname: "Wayne",
        age: 43,
        address:{
          street: `50 Wayne Manor`,
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/batman.png",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("08/07/1979 12:00:00"),
        hide: false
      }
    ]; // end of array
  }; // end of constructor

  // METHODS
  // create a method that will return a array of this.characters
  // using am observable
  // OBSERVABLES
  /*
  provides support for passing messages/data between parts of your application
  used frequently and is a technique for event handling, asynchronous programming,
  and handling multiple values.
   */
  getCharacters() : Observable<DChero[]> {
    console.log("Getting characters from data service")
    return of(this.characters);
  }

} // end of class
