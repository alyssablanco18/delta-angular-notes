import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Comment} from "../models/Comment";
const httpOptions = {
  headers: new HttpHeaders({"Content-Type": "application/json"})
}
@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  //properties
  commentsURL: string = "https://jsonplaceholder.typicode.com/comments"
  constructor(private httpClient: HttpClient) { }
  getComment(): Observable<Comment[]>{
    return this.httpClient.get<Comment[]>(this.commentsURL)
  }
  saveComment(comment: Comment) : Observable<Comment> {
    return this.httpClient.post<Comment>(this.commentsURL,comment,httpOptions)
  }
  // create a method that will make a PUT request to the commentsURL property
  updateComment(commentUpdated: Comment) : Observable<Comment>{
    const url = `${this.commentsURL}/${commentUpdated.id}`;
    return this.httpClient.put<Comment>(url, commentUpdated, httpOptions)
  }
  // create a method that will make a DELETE request to the url
  deleteComment(commentDeleted: Comment | number): Observable<Comment> {
    const id = typeof commentDeleted === `number`? commentDeleted : commentDeleted.id;
    const url = `${this.commentsURL}/${id}`
    return this.httpClient.delete<Comment>(url, httpOptions);
  }
}
