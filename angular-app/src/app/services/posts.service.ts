import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostHttpClient} from "../models/PostHttpClient";

const httpOptions = {
  headers: new HttpHeaders({"Content-type": "application/json"})
}
// setting up our header value to the content-type of application/json
@Injectable({
  providedIn: 'root'
})
export class PostsService {
  // PROPERTIES

  // set a url as a property
  postUrl: string = "https://jsonplaceholder.typicode.com/posts";

  // inject the HttpClientModule as a dependency
  constructor(private httpClient: HttpClient) {
  }

  // create a method that will make our GET request to the postUrl
  getPosts(): Observable<PostHttpClient[]> {

    // returning all the data that comes with our postUrl property
    return this.httpClient.get<PostHttpClient[]>(this.postUrl);
  }

  // create a method that will make a POST request to the posturl property
  savePost(post: PostHttpClient): Observable<PostHttpClient> {
    return this.httpClient.post<PostHttpClient>(this.postUrl, post, httpOptions);
  }

  // create a method that will make a PUT request to the postUrl property
  updatePost(postUpdated: PostHttpClient): Observable<PostHttpClient> {
    const url = `${this.postUrl} / {postUpdated.id}`;

    return this.httpClient.put<PostHttpClient>(url, postUpdated, httpOptions)
  }

  /*
  1. created a new const variable 'url' - this is taking the json url
  and the id of the selected post.
  2. returning everything as a PUT request, not as a POST
  3. "PUTTING" this on the 'const url' NOT on the 'this.postUrl'
   */

  // create a method that will make a delete request to the url
  // @ts-ignore
  deletePost(postDeleted: PostHttpClient | number): Observable<PostHttpClient> {
      const id = typeof postDeleted === `number`? postDeleted : postDeleted.id;

      /*
      if whats being passed in is a type of number its postDeleted ( our perameter )
      otherwise use the id of 'postDeleted'
       */

    const url = `${this.postUrl}/${id}`

    return this.httpClient.delete<PostHttpClient>(url, httpOptions);
  }




} // end of export class
